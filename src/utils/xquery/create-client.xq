
import module namespace c = 'urn:nubisware:keycloak:clients' at 'keycloak-clients.xqm';
import module namespace gw = 'urn:nubisware:d4science:gateways' at 'd4s-gateways.xqm';


declare function local:build-clientId($gateway) {
  replace($gateway, 'https://', '')
};

declare function local:build-client-def($gateway) {
  let $clientId := local:build-clientId($gateway)
  let $baseUrl := $gateway || '/'
  let $redirectUri := $gateway || '/*'
  let $login_theme := "d4science"
  return map {
    "clientId" : $clientId,
    "baseUrl" : $baseUrl,
    "redirectUri" : $redirectUri,
    "login_theme" : $login_theme
  }
};

let $dummy := ''

(: delete all clients
 gw:list() ! c:delete-client(local:build-clientId(.))
:)

(: add all clients
 gw:list() ! c:create-client(local:build-client-def(.)) 
:)

return gw:list() ! c:create-client(local:build-client-def(.)) 