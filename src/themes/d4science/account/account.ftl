<#import "template.ftl" as layout>
<@layout.mainLayout active='account' bodyClass='user'; section>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("editAccountHtmlTitle")}</h2>
        </div>
        <div class="col-md-2 subtitle">
            <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
        </div>
    </div>

    <form action="${url.accountUrl}" class="form-horizontal" method="post">

        <input type="hidden" id="stateChecker" name="stateChecker" value="${stateChecker}">

        <#if !realm.registrationEmailAsUsername>
            <div class="form-group ${messagesPerField.printIfExists('username','has-error')}">
                <div class="col-sm-2 col-md-2">
                    <label for="username" class="control-label">${msg("username")}</label> <#if realm.editUsernameAllowed><span class="required">*</span></#if>
                </div>

                <div class="col-sm-10 col-md-10">
                    <input type="text" class="form-control" id="username" name="username" <#if !realm.editUsernameAllowed>disabled="disabled"</#if> value="${(account.username!'')}"/>
                </div>
            </div>
        </#if>

        <div class="form-group ${messagesPerField.printIfExists('email','has-error')}">
            <div class="col-sm-2 col-md-2">
            <label for="email" class="control-label">${msg("email")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="email" name="email" autofocus value="${(account.email!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('firstName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="firstName" class="control-label">${msg("firstName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="firstName" name="firstName" value="${(account.firstName!'')}"/>
            </div>
        </div>

        <div class="form-group ${messagesPerField.printIfExists('lastName','has-error')}">
            <div class="col-sm-2 col-md-2">
                <label for="lastName" class="control-label">${msg("lastName")}</label> <span class="required">*</span>
            </div>

            <div class="col-sm-10 col-md-10">
                <input type="text" class="form-control" id="lastName" name="lastName" value="${(account.lastName!'')}"/>
            </div>
        </div>

        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <#if url.referrerURI??><a href="${url.referrerURI}">${kcSanitize(msg("backToApplication")?no_esc)}</a></#if>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Cancel">${msg("doCancel")}</button>
                </div>
            </div>
        </div>
    </form>

    <div class="row">
        <div class="col-md-10">
            <h2>${msg("changeAvatarHtmlTitle")}</h2>
        </div>
    </div>

    <#assign avatarUrl = url.accountUrl?replace("^(.*)(/account/?)(\\?(.*))?$", "$1/avatar-provider/?account&$4", 'r') />
    <form id="theForm" action="${avatarUrl}" class="form-horizontal" method="post" enctype="multipart/form-data">
 
        <img src="${avatarUrl}" style="max-width: 200px;"
          onerror="let div=document.createElement('div');div.id='avatarInfo';div.innerHTML='${msg("noAvatarSet")}<br/>${msg("avatarFileSizeMessage")}';this.replaceWith(div)" />
        
        <div id="avatarError" class="alert alert-danger" style="margin-top: 0; display: none;">
          <span class="pficon pficon-error-circle-o"></span>
          <strong>${msg("avatarFileTooBig")}</strong> ${msg("avatarFileSizeMessage")}
        </div>

        <input style="margin-top: 1em;" type="file" id="avatar" name="imageSelector">

        <input type="hidden" name="stateChecker" value="${stateChecker}">

        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <button type="button" onClick="sendAvatar()" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="Save">${msg("doSave")}</button>
                </div>
            </div>
        </div>

        <script>
          function sendAvatar() {
            var filesToUpload = avatar.files;
            var file = filesToUpload[0];

            // Create an image
            var img = document.createElement("img");
            // Create a file reader
            var reader = new FileReader();
            // Set the image once loaded into file reader
            reader.onload = function(e) {
                img.src = e.target.result;

                img.onload = function () {
                    var canvas = document.createElement("canvas");
                    var ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0);

                    var MAX_WIDTH = 250;
                    var MAX_HEIGHT = 250;
                    var width = img.width;
                    var height = img.height;

                    if (width > height) {
                      if (width > MAX_WIDTH) {
                        height *= MAX_WIDTH / width;
                        width = MAX_WIDTH;
                      }
                    } else {
                      if (height > MAX_HEIGHT) {
                        width *= MAX_HEIGHT / height;
                        height = MAX_HEIGHT;
                      }
                    }
                    canvas.width = width;
                    canvas.height = height;
                    ctx = canvas.getContext("2d");
                    ctx.drawImage(img, 0, 0, width, height);

                    canvas.toBlob(function(blob) {
                        var form = document.getElementById('theForm')
                        var formData = new FormData(form)
                        // Deleting the big image from the form data to prevent send
                        formData.delete('imageSelector')
                        // Adding new blob with new image with right size
                        formData.append("image", blob);
                        var xhr = new XMLHttpRequest();
                        xhr.onreadystatechange = function() {
                          if (xhr.readyState == 4 && xhr.status == 200) {
                            location.reload();
                          }
                        };
                        xhr.open(form.method, form.action, true);
                        xhr.send(formData);
                    });
                } // img.onload
            }
            // Load files into file reader
            reader.readAsDataURL(file);
          }
        </script>

    </form>


    <div class="row">
        <div class="col-md-10">
            <h2>${msg("deleteAccountHtmlTitle")}</h2>
        </div>
    </div>

    <#assign deleteUrl = url.accountUrl?replace("^(.*)(/account/?)(\\?(.*))?$", "$1/delete-account/delete?$4", 'r') />
    <form action="${deleteUrl}" class="form-horizontal" method="post" onsubmit="return confirm('${msg("deleteAccountConfirmDeleteMessage")}');" >

        <input type="hidden" name="stateChecker" value="${stateChecker}">

        <div class="form-group">
            <div class="col-md-12">${msg("deleteAccountMessage")}</div>
        </div>
        <div class="form-group" style="border: 1px solid #f1d875; background: #fffbdc">
            <div class="col-md-1" style="font-weight: bold; color: #bf7900;">${msg("deleteAccountWarningTitle")}</div>
            <div class="col-md-11">${msg("deleteAccountWarningMessage")}</div>
        </div>
        <div class="form-group">
            <div id="kc-form-buttons" class="col-md-offset-2 col-md-10 submit">
                <div class="">
                    <button type="submit" class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="submitAction" value="${msg("deleteAccountSubmitButton")}">${msg("deleteAccountSubmitButton")}</button>
                </div>
            </div>
        </div>
    </form>

</@layout.mainLayout>
