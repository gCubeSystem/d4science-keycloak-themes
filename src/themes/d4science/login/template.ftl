<#macro registrationLayout bodyClass="" displayInfo=false displayMessage=true displayRequiredFields=false displayWide=false showAnotherWayIfPresent=true>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" class="${properties.kcHtmlClass!}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="robots" content="noindex, nofollow"><!-- base -->

    <#if properties.meta?has_content>
        <#list properties.meta?split(' ') as meta>
            <meta name="${meta?split('==')[0]}" content="${meta?split('==')[1]}"/>
        </#list>
    </#if>
    <title><#if properties.titleTag?has_content>${properties.titleTag}<#else>${msg("loginTitle",(realm.displayName!''))}</#if></title>
    <link rel="icon" href="<#if properties.favicon?has_content>${properties.favicon}<#else>${url.resourcesPath}/img/favicon.ico</#if>" />
    <#if properties.styles?has_content>
        <#list properties.styles?split(' ') as style>
            <link href="${url.resourcesPath}/${style}" rel="stylesheet" />
        </#list>
    </#if>
    <#if properties.scripts?has_content>
        <#list properties.scripts?split(' ') as script>
            <script src="${url.resourcesPath}/${script}" type="text/javascript"></script>
        </#list>
    </#if>
    <#if scripts??>
        <#list scripts as script>
            <script src="${script}" type="text/javascript"></script>
        </#list>
    </#if>
</head>

<body class="${properties.kcBodyClass!}">
<div id="content" style="<#if properties.contentBgImg?has_content>background: url('${url.resourcesPath}/${properties.contentBgImg!}') no-repeat center center fixed; background-size: cover;</#if> ${properties.contentStyle!}">

  <div class="${properties.kcLoginClass!}">
    <!--
    <div id="kc-header" class="${properties.kcHeaderClass!}">
      <div id="kc-header-wrapper" class="${properties.kcHeaderWrapperClass!}">${kcSanitize(msg("loginTitleHtml",(realm.displayNameHtml!'')))?no_esc}</div>
    </div>
    -->
 
    <div class="${properties.kcFormCardClass!} <#if displayWide>${properties.kcFormCardAccountClass!}</#if>">

      <div style="${properties.logoHeaderStyle!}">
        <div>
          <#if client?? && client.getClientId()!='account'>
            <a href="<#if client.getBaseUrl()?has_content>${client.getBaseUrl()}<#else>${"https://" + client.getClientId()}</#if>">
              <#if properties.logoSrc?has_content>
                <img class="img-fluid float-left" alt="${properties.logoAlt!}" src="${properties.logoSrc!}" style="${properties.logoStyle!}">
              <#else>
                <h1>
                  <#if client.getName()?has_content && client.getName()?starts_with("${")>${kcSanitize(msg(client.getName()?keep_after("{")?keep_before("}")))}
                  <#else>${(client.getName()!client.getClientId()!"undefined client")?capitalize?keep_before('.')}</#if>
                </h1>
              </#if>
            </a>
          </#if>
        </div>
				<#if properties.infrastructureLogo?has_content && properties.infrastructureLogo='yes'>
          <div>
            <a target="_blank" href="http://www.d4science.org">
              <img class="img-fluid float-right" alt="D4Science Infrastructure" src="${url.resourcesPath}/img/PoweredByD4Science.png" style="${properties.infrastructureLogoStyle!}">
            </a>
          </div>
				</#if>
      </div>

      <header class="${properties.kcFormHeaderClass!}">
        <#if realm.internationalizationEnabled  && locale.supported?size gt 1>
            <div id="kc-locale">
                <div id="kc-locale-wrapper" class="${properties.kcLocaleWrapperClass!}">
                    <div class="kc-dropdown" id="kc-locale-dropdown">
                        <a href="#" id="kc-current-locale-link">${locale.current}</a>
                        <ul>
                            <#list locale.supported as l>
                                <li class="kc-dropdown-item"><a href="${l.url}">${l.label}</a></li>
                            </#list>
                        </ul>
                    </div>
                </div>
            </div>
        </#if>
        <#if !(auth?has_content && auth.showUsername() && !auth.showResetCredentials())>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <h1 id="kc-page-title"><#nested "header"></h1>
                    </div>
                </div>
            <#else>
                <h1 id="kc-page-title"><#nested "header"></h1>
            </#if>
        <#else>
            <#if displayRequiredFields>
                <div class="${properties.kcContentWrapperClass!}">
                    <div class="${properties.kcLabelWrapperClass!} subtitle">
                        <span class="subtitle"><span class="required">*</span> ${msg("requiredFields")}</span>
                    </div>
                    <div class="col-md-10">
                        <#nested "show-username">
                        <div class="${properties.kcFormGroupClass!}">
                            <div id="kc-username">
                                <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                                <a id="reset-login" href="${url.loginRestartFlowUrl}">
                                    <div class="kc-login-tooltip">
                                        <i class="${properties.kcResetFlowIcon!}"></i>
                                        <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            <#else>
                <#nested "show-username">
                <div class="${properties.kcFormGroupClass!}">
                    <div id="kc-username">
                        <label id="kc-attempted-username">${auth.attemptedUsername}</label>
                        <a id="reset-login" href="${url.loginRestartFlowUrl}">
                            <div class="kc-login-tooltip">
                                <i class="${properties.kcResetFlowIcon!}"></i>
                                <span class="kc-tooltip-text">${msg("restartLoginTooltip")}</span>
                            </div>
                        </a>
                    </div>
                </div>
            </#if>
        </#if>
      </header>
      <div id="kc-content">
        <div id="kc-content-wrapper">

          <#-- App-initiated actions should not see warning messages about the need to complete the action -->
          <#-- during login.                                                                               -->
          <#if displayMessage && message?has_content && (message.type != 'warning' || !isAppInitiatedAction??)>
              <div class="alert alert-${message.type}">
                  <#if message.type = 'success'><span class="${properties.kcFeedbackSuccessIcon!}"></span></#if>
                  <#if message.type = 'warning'><span class="${properties.kcFeedbackWarningIcon!}"></span></#if>
                  <#if message.type = 'error'><span class="${properties.kcFeedbackErrorIcon!}"></span></#if>
                  <#if message.type = 'info'><span class="${properties.kcFeedbackInfoIcon!}"></span></#if>
                  <span class="kc-feedback-text">${kcSanitize(message.summary)?no_esc}</span>
              </div>
          </#if>

          <#nested "form">

          <#if auth?has_content && auth.showTryAnotherWayLink() && showAnotherWayIfPresent>
          <form id="kc-select-try-another-way-form" action="${url.loginAction}" method="post" <#if displayWide>class="${properties.kcContentWrapperClass!}"</#if>>
              <div <#if displayWide>class="${properties.kcFormSocialAccountContentClass!} ${properties.kcFormSocialAccountClass!}"</#if>>
                  <div class="${properties.kcFormGroupClass!}">
                    <input type="hidden" name="tryAnotherWay" value="on" />
                    <a href="#" id="try-another-way" onclick="document.forms['kc-select-try-another-way-form'].submit();return false;">${msg("doTryAnotherWay")}</a>
                  </div>
              </div>
          </form>
          </#if>

          <#if displayInfo>
              <div id="kc-info" class="${properties.kcSignUpClass!}">
                  <div id="kc-info-wrapper" class="${properties.kcInfoAreaWrapperClass!}">
                      <#nested "info">
                  </div>
              </div>
          </#if>
        </div>

      </div>

      <footer style="${properties.footerStyle!}">
        <div>
          <a href="${properties.linkTerms!}">Terms of Use</a> | 
          <a href="${properties.linkCookies!'#'}">Cookies Policy</a> | 
          <a href="${properties.linkPrivacy!'#'}" target="_blank">Privacy Policy</a> | 
          <#if properties.linkProject?has_content><a href="${properties.linkProject!}" target="_blank">${properties.descrProject!}</a></#if>
        </div>
				<#if properties.ECLogo?has_content && properties.ECLogo='yes'>
          <div style="display: flex; padding-top: 10px; justify-content: space-between;">
            <#if properties.footerRow?has_content><div>${kcSanitize(properties.footerRow)?no_esc}</div></#if>
            <div style="align-self: center;">
              <a href="http://ec.europa.eu/programmes/horizon2020/" target="_blank">
                <img class="float-right" alt="${properties.ECLogoAlt!}" src="${url.resourcesPath}/img/logo-ec.jpg" style="${properties.ECLogoStyle!}">
              </a>
            </div>
          </div>
        <#else>
          <#if properties.footerRow?has_content><div style="padding-top: 10px;">${kcSanitize(properties.footerRow)?no_esc}</div></#if>
        </#if>
      </footer>

    </div><!-- end form card -->
  </div><!-- end login -->

</div><!-- end content -->
</body>
</html>
</#macro>
