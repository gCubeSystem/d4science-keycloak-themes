<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false displayWide=true; section>
    <#if section = "header">
        ${msg("termsTitle")}
    <#elseif section = "form">
        <div id="kc-terms-text" style="max-height: 40vh; overflow-y: auto; padding: 0 10px 10px 0;">
            <#include "terms.html" parse=false>
        </div>
        <div style="text-align: right; font-weight: bold; font-variant: small-caps;"><p>${msg("termsAcceptMsg")}</p></div>
        <form class="form-actions" action="${url.loginAction}" method="POST">
            <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="accept" id="kc-accept" type="submit" value="${msg("doAccept")}" disabled />
            <input class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="cancel" id="kc-decline" type="submit" value="${msg("doDecline")}"/>
        </form>
        <div class="clearfix"></div>
        <script type="text/javascript">
          document.getElementById("kc-terms-text").addEventListener("scroll", checkKcTermsScrollHeight, false);

          function checkKcTermsScrollHeight(){
            var kcTermsTextElement = document.getElementById("kc-terms-text")
            if ((kcTermsTextElement.scrollTop + kcTermsTextElement.offsetHeight + 5) >= kcTermsTextElement.scrollHeight){
              document.getElementById("kc-accept").disabled = false;
            }
          }
        </script>
    </#if>
</@layout.registrationLayout>
