# D4Science Keycloak Themes

# [DEPRECATED]
## This repo is now obsolete/unmantained and its base code and all D4Science's GW themes are now available into the [keycloak-d4science-spi-parent/keycloak-d4science-theme](https://code-repo.d4science.org/gCubeSystem/keycloak-d4science-spi-parent/src/branch/master/keycloak-d4science-theme) repo. In this new repo, themes can now be built as JAR, by using the included `maven` script, for an easier deployment.
##

**D4Science Keycloak Themes** repository collects the implementations of base D4Science theme and a set of specific per gateway themes implementations.

Each Keycloak theme is made of a set of [Freemarker](https://freemarker.apache.org/) templates.

## Structure of the project

The source code is present in `src` folder.
The themes are in `src/themes` subfolder.
The `src/utils` subfolder contains some utility test scripts to create Keycloak clients that uses a specific theme.

## Built With

Not applicable.

## Documentation

For details see [Theme section](https://www.keycloak.org/docs/latest/server_development/#_themes) of Keycloak developer docs.

As specified in [Deploying Themes](https://www.keycloak.org/docs/latest/server_development/#deploying-themes) section of the documentation, *themes can be deployed to Keycloak by copying the theme directory to themes or it can be deployed as an archive.*

In a future release of this project, when themes have been fully tested, a script to build a themes archive will be developed. Currently, in order to deploy themes in a Keycloak instance, the first approach is applied.

During the development phase, themes caching in Keycloak configuration file has to be disabled. To do this edit `standalone.xml`. For `theme` set `staticMaxAge` to `-1` and both `cacheTemplates` and `cacheThemes` to `false`. 

At this point it is suggested to clone this repo somewhere and create a symbolic link for each theme you want to deploy:

    mkdir /opt/git
    cd /opt/git
    git clone https://code-repo.d4science.org/gCubeSystem/d4science-keycloak-themes.git
    
    cd /opt/keycloak/themes
    ln -s /opt/git/d4science-keycloak-themes/src/themes/d4science/ 
    ln -s /opt/git/d4science-keycloak-themes/src/themes/dev4.d4science.org/
    ln -s /opt/git/d4science-keycloak-themes/src/themes/next.d4science.org/

If you are already logged-in in Keycloak administration console you have to logout and then re-login. At that you can set one of the new themes per Keycloak Realm or for a specific Keycloak Client in the realm.

## Change log

See [Releases](https://code-repo.d4science.org/gCubeSystem/d4science-keycloak-themes/releases).

## Authors

* **Vincenzo Cestone** ([Nubisware S.r.l.](http://www.nubisware.com))

## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes including:

- the Sixth Framework Programme for Research and Technological Development
    - DILIGENT (grant no. 004260);
- the Seventh Framework Programme for research, technological development and demonstration 
    - D4Science (grant no. 212488), D4Science-II (grant no.239019), ENVRI (grant no. 283465), EUBrazilOpenBio (grant no. 288754), iMarine(grant no. 283644);
- the H2020 research and innovation programme 
    - BlueBRIDGE (grant no. 675680), EGIEngage (grant no. 654142), ENVRIplus (grant no. 654182), Parthenos (grant no. 654119), SoBigData (grant no. 654024),DESIRA (grant no. 818194), ARIADNEplus (grant no. 823914), RISIS2 (grant no. 824091), PerformFish (grant no. 727610), AGINFRAplus (grant no. 731001);
    