# Changelog for "d4science-keycloak-themes"

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- First release 

[#19518] Implemented the Keycloak "d4science" login parametric template.

Each new gateway that does not implement its own specific login template should use by default this base "d4science" template. However, it is possible to set some parameters in `<keycloak_home>/themes/<gatewayname>/login/themes.properties` file to do some customizations such as logo, background, colors, and some links (see for example parameters of dev4.d4science.org and next.d4science.org themes).


